<?php

namespace App\Services;

use App\Model\Tariff;

class TariffLoyaltyCard
{
    private $discount = 20;

    public function apply(Tariff $tariff): Tariff
    {
        $tariff->applyDiscount($this->discount);

        return $tariff;
    }

}