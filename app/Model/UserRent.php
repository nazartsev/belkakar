<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserRent extends Model
{
    protected $table = 'user_rent';

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'car_id',
        'tariff_id',
        'rent_start',
        'rent_finish',
    ];
}
