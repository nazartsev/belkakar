<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tariff extends Model
{
    protected $table = 'tariffs';

    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function options()
    {
        return $this->hasMany(TariffOption::class);
    }

    public function steps()
    {
        return $this->hasMany(TariffStep::class);
    }

    public function applyDiscount(float $discount)
    {
        foreach ($this->options as $option) {
            $option->applyDiscount($discount);
        }

        foreach ($this->steps as $step) {
            $step->applyDiscount($discount);
        }
    }
}
