<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $table = 'options';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'price',
        'limit',
    ];
}
