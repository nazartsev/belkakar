<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TariffStepType extends Model
{
    protected $table = 'tariff_step_types';

    public $timestamps = false;

    protected $fillable = [
        'tariff_step_id', 'rule_type_id', 'price', 'limit'
    ];

    public function tariffStep()
    {
        return $this->hasOne(TariffStep::class, 'id', 'tariff_step_id');
    }

    public function ruleType()
    {
        return $this->hasOne(RuleType::class, 'id', 'rule_type_id');
    }

    public function rules()
    {
        return $this->hasMany(TariffRule::class, 'tariff_step_type_id', 'id');
    }

    private function getPrice()
    {
        $this->price = $this->price ?? $this->ruleType->price;
    }

    public function applyDiscount(float $discount)
    {
        $this->getPrice();
        $this->price = $this->price  - round($discount / 100 * $this->price);
        foreach ($this->rules as $rule) {
            $rule->applyDiscount($discount);
        }
    }
}
