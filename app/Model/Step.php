<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    protected $table = 'steps';

    public $timestamps = false;

    protected $fillable = [
        'name', 'code'
    ];
}
