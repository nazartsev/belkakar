<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TariffStep extends Model
{
    protected $table = 'tariff_steps';

    public $timestamps = false;

    protected $fillable = [
        'tariff_id', 'step_id'
    ];

    public function tariff()
    {
        return $this->hasOne(Tariff::class, 'id', 'tariff_id');
    }

    public function step()
    {
        return $this->hasOne(Step::class, 'id', 'step_id');
    }

    public function types()
    {
        return $this->hasMany(TariffStepType::class, 'tariff_step_id', 'id');
    }

    public function applyDiscount(float $discount)
    {
        foreach ($this->types as $type) {
            $type->applyDiscount($discount);
        }
    }
}
