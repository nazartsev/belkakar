<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TariffRule extends Model
{
    protected $table = 'tariff_rules';

    public $timestamps = false;

    protected $fillable = [
        'tariff_step_type_id',
        'work_time_from',
        'work_time_to',
        'price',
        'count_from',
        'count_to',
        'minus'
    ];

    public function tariffStepType()
    {
        return $this->hasOne(TariffStepType::class);
    }

    public function applyDiscount(float $discount)
    {
        $this->price = $this->price  - round($discount / 100 * $this->price);
    }
}
