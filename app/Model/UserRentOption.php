<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserRentOption extends Model
{
    protected $table = 'user_rent_options';

    public $timestamps = false;

    protected $fillable = [
        'user_rent_id',
        'option_id',
        'option_start',
        'option_finish',
        'amount',
        'cost',
        'price'
    ];
}
