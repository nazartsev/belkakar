<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserRentSteps extends Model
{
    protected $table = 'user_rent_steps';

    public $timestamps = false;

    protected $fillable = [
        'user_rent_id',
        'tariff_step_type_id',
        'step_start',
        'step_finish',
        'amount',
        'cost',
        'price'
    ];
}
