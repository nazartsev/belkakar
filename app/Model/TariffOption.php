<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TariffOption extends Model
{
    protected $table = 'tariff_options';

    public $timestamps = false;

    protected $fillable = [
        'tariff_id',
        'option_id',
        'price',
        'limit',
    ];

    public function tariff()
    {
        return $this->hasOne(Tariff::class, 'id', 'tariff_id');
    }

    public function option()
    {
        return $this->hasOne(Option::class, 'id', 'option_id');
    }

    public function getPrice()
    {
        return $this->price ?? $this->option->price;
    }

    public function getLimit()
    {
        return $this->limit ?? $this->option->limit;
    }

    public function getName()
    {
        return $this->option->name;
    }

    public function applyDiscount(float $discount)
    {
        $this->price = $this->getPrice() - round($this->getPrice() * $discount / 100 );
    }
}
