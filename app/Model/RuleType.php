<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RuleType extends Model
{
    protected $table = 'rule_types';

    public $timestamps = false;

    protected $fillable = [
        'name', 'code'
    ];
}
