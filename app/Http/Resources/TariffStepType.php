<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\TariffStepTypeRule as TariffStepTypeRuleResource;

class TariffStepType extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            "code" => $this->ruleType->code,
            "price" => $this->price ?? $this->ruleType->price,
            "limit" => $this->limit ?? $this->ruleType->limit,
            "rules" => TariffStepTypeRuleResource::collection($this->rules)
        ];
    }
}
