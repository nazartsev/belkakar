<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\TariffOption as TariffOptionResource;
use App\Http\Resources\TariffStep as TariffStepResource;

class Tariff extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'options' => TariffOptionResource::collection($this->options),
            'steps' => TariffStepResource::collection($this->steps)
        ];
    }
}
