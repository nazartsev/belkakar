<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\TariffStepType as TariffStepTypeResource;

class TariffStep extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->step->code,
            'types' => $this->getStepTypes($this->types)
        ];
    }

    private function getStepTypes($types)
    {
        $collection = [];
        foreach ($types as $type) {
            $collection[$type->ruleType->code] = new TariffStepTypeResource($type);
        }

        return $collection;
    }
}
