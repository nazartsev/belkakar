<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TariffStepTypeRule extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'work_time' => [
                'from' => $this->work_time_from,
                'to' => $this->work_time_to,
            ],
            'count' => [
                'from' => $this->count_from,
                'to' => $this->count_to,
            ],
            'price' => $this->price,
            'minus' => $this->minus
        ];
    }
}
