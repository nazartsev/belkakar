<?php

namespace App\Http\Controllers;

use App\Http\Resources\Tariff as TariffResource;
use App\Model\Tariff;
use App\Services\TariffLoyaltyCard;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TariffController extends Controller
{
    private $loyaltyCard;

    public function __construct(TariffLoyaltyCard $loyaltyCard)
    {
        $this->loyaltyCard = $loyaltyCard;
    }

    public function all()
    {
        $tariffs = Tariff::all();

        return TariffResource::collection(
            $tariffs->map(
                function($tariff) {
                    return $this->loyaltyCard->apply($tariff);
                }
            )
        );
    }

    public function show($id)
    {
        try {
            $tariff = Tariff::find($id);
            if ($tariff) {
                return new TariffResource($this->loyaltyCard->apply($tariff));
            }
            return response()->json(['error' => 400, 'message' => sprintf('Tariff not found with id %s', (string)$id)]);
        } catch (\Exception $exception) {
            return response()->json(['error' => 400, 'message' => $exception->getMessage()]);
        }
    }
}
