<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WorkFlow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('user_rent', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('car_id');
            $table->integer('tariff_id');
            $table->dateTime('rent_start');
            $table->dateTime('rent_finish')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('car_id')->references('id')->on('cars');
            $table->foreign('tariff_id')->references('id')->on('tariffs');
        });

        Schema::create('user_rent_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_rent_id');
            $table->integer('option_id');
            $table->dateTime('option_start');
            $table->dateTime('option_finish')->nullable();
            $table->float('price');
            $table->float('amount')->nullable();
            $table->float('cost')->nullable();

            $table->foreign('user_rent_id')->references('id')->on('user_rent');
            $table->foreign('option_id')->references('id')->on('tariff_options');
        });

        Schema::create('user_rent_steps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_rent_id');
            $table->integer('tariff_step_type_id');
            $table->dateTime('step_start');
            $table->dateTime('step_finish')->nullable();
            $table->float('price');
            $table->float('amount')->nullable();
            $table->float('cost')->nullable();

            $table->foreign('user_rent_id')->references('id')->on('user_rent');
            $table->foreign('tariff_step_type_id')->references('id')->on('tariff_step_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_rent_steps');
        Schema::dropIfExists('user_rent_options');
        Schema::dropIfExists('user_rent');
        Schema::dropIfExists('cars');
    }
}
