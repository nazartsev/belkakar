<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Fixture extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $option = new \App\Model\Option([
            'name' => 'Детское кресло',
            'price' => 1,
            'limit' => 300,
        ]);
        $option->save();

        $tariff = new \App\Model\Tariff(['name' => 'Базовый тариф']);
        $tariff->save();

        $tariffOption = new \App\Model\TariffOption([
            'option_id' => $option->getAttribute('id'),
            'price' => null,
            'limit' => null,
        ]);
        $tariff->options()->save($tariffOption);

        $stepReservation = new \App\Model\Step(['name' => 'Бронирование', 'code' => 'reservation']);
        $stepReservation->save();

        $stepInspection = new \App\Model\Step(['name' => 'Осмотр', 'code' => 'inspection']);
        $stepInspection->save();

        $stepDrive = new \App\Model\Step(['name' => 'Поездка', 'code' => 'drive']);
        $stepDrive->save();

        $stepParking = new \App\Model\Step(['name' => 'Парковка', 'code' => 'parking']);
        $stepParking->save();

        $stepTypeTime = new \App\Model\RuleType(['name' => 'Время', 'code' => 'time']);
        $stepTypeTime->save();

        $stepTypeLength = new \App\Model\RuleType(['name' => 'Дистанция', 'code' => 'length']);
        $stepTypeLength->save();

        $tariffStepReservation = new \App\Model\TariffStep([
            'tariff_id' => $tariff->getAttribute('id'),
            'step_id' => $stepReservation->getAttribute('id'),
        ]);
        $tariffStepReservation->save();

        $tariffStepReservationType = new \App\Model\TariffStepType([
            'tariff_step_id' => $tariffStepReservation->getAttribute('id'),
            'rule_type_id' => $stepTypeTime->getAttribute('id'),
            'price' => 0,
            'limit' => null
        ]);
        $tariffStepReservationType->save();

        $tariffStepReservationRule = new \App\Model\TariffRule([
            'tariff_step_type_id' => $tariffStepReservationType->getAttribute('id'),
            'work_time_from' => '7:00',
            'work_time_to' => '23:00',
            'price' => 2,
            'count_from' => 20,
            'count_to' => null,
            'minus' => 20
        ]);
        $tariffStepReservationRule->save();

        $tariffStepInspection = new \App\Model\TariffStep([
            'tariff_id' => $tariff->getAttribute('id'),
            'step_id' => $stepInspection->getAttribute('id'),
        ]);
        $tariffStepInspection->save();

        $tariffStepInspectionType = new \App\Model\TariffStepType([
            'tariff_step_id' => $tariffStepInspection->getAttribute('id'),
            'rule_type_id' => $stepTypeTime->getAttribute('id'),
            'price' => 0,
            'limit' => null
        ]);
        $tariffStepInspectionType->save();

        $tariffStepInspectionRule = new \App\Model\TariffRule([
            'tariff_step_type_id' => $tariffStepInspectionType->getAttribute('id'),
            'work_time_from' => '0:00',
            'work_time_to' => '24:00',
            'price' => 2,
            'count_from' => 7,
            'count_to' => null,
            'minus' => 7
        ]);
        $tariffStepInspectionRule->save();

        $tariffStepDrive = new \App\Model\TariffStep([
            'tariff_id' => $tariff->getAttribute('id'),
            'step_id' => $stepDrive->getAttribute('id'),
        ]);
        $tariffStepDrive->save();

        $tariffStepDriveTypeTime = new \App\Model\TariffStepType([
            'tariff_step_id' => $tariffStepDrive->getAttribute('id'),
            'rule_type_id' => $stepTypeTime->getAttribute('id'),
            'price' => 8,
            'limit' => 2700
        ]);
        $tariffStepDriveTypeTime->save();

        $tariffStepDriveTypeLength = new \App\Model\TariffStepType([
            'tariff_step_id' => $tariffStepDrive->getAttribute('id'),
            'rule_type_id' => $stepTypeLength->getAttribute('id'),
            'price' => 0,
            'limit' => null
        ]);
        $tariffStepDriveTypeLength->save();

        $tariffStepDriveLengthRule = new \App\Model\TariffRule([
            'tariff_step_type_id' => $tariffStepDriveTypeLength->getAttribute('id'),
            'work_time_from' => '0:00',
            'work_time_to' => '24:00',
            'price' => 10,
            'count_from' => 70,
            'count_to' => null,
            'minus' => 70
        ]);
        $tariffStepDriveLengthRule->save();

        $tariffStepParking = new \App\Model\TariffStep([
            'tariff_id' => $tariff->getAttribute('id'),
            'step_id' => $stepParking->getAttribute('id'),
        ]);
        $tariffStepParking->save();

        $tariffStepParkingType = new \App\Model\TariffStepType([
            'tariff_step_id' => $tariffStepParking->getAttribute('id'),
            'rule_type_id' => $stepTypeTime->getAttribute('id'),
            'price' => 0,
            'limit' => null
        ]);
        $tariffStepParkingType->save();

        $tariffStepParkingRule = new \App\Model\TariffRule([
            'tariff_step_type_id' => $tariffStepParkingType->getAttribute('id'),
            'work_time_from' => '7:00',
            'work_time_to' => '23:00',
            'price' => 2,
            'count_from' => 0,
            'count_to' => null,
            'minus' => 0
        ]);
        $tariffStepParkingRule->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
