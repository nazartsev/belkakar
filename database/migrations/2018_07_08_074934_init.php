<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Init extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tariffs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('options', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->float('price');
            $table->float('limit');
        });

        Schema::create('tariff_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tariff_id');
            $table->integer('option_id');
            $table->float('price')->nullable();
            $table->float('limit')->nullable();

            $table->foreign('tariff_id')->references('id')->on('tariffs')->onDelete('cascade');;
            $table->foreign('option_id')->references('id')->on('options')->onDelete('cascade');;
        });

        Schema::create('steps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code');
        });

        Schema::create('tariff_steps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tariff_id');
            $table->integer('step_id');

            $table->foreign('tariff_id')->references('id')->on('tariffs')->onDelete('cascade');;
            $table->foreign('step_id')->references('id')->on('steps')->onDelete('cascade');;
        });

        Schema::create('rule_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code');
        });

        Schema::create('tariff_step_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tariff_step_id');
            $table->integer('rule_type_id');
            $table->float('price')->nullable();
            $table->float('limit')->nullable();

            $table->foreign('tariff_step_id')->references('id')->on('tariff_steps')->onDelete('cascade');;
            $table->foreign('rule_type_id')->references('id')->on('rule_types')->onDelete('cascade');;
        });

        Schema::create('tariff_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tariff_step_type_id');
            $table->string('work_time_from');
            $table->string('work_time_to');
            $table->float('price');
            $table->float('count_from')->nullable();
            $table->float('count_to')->nullable();
            $table->float('minus')->nullable();

            $table->foreign('tariff_step_type_id')->references('id')->on('tariff_step_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tariff_rules');
        Schema::dropIfExists('tariff_step_types');
        Schema::dropIfExists('rule_types');
        Schema::dropIfExists('tariff_steps');
        Schema::dropIfExists('steps');
        Schema::dropIfExists('tariff_options');
        Schema::dropIfExists('options');
        Schema::dropIfExists('tariff');
    }
}
