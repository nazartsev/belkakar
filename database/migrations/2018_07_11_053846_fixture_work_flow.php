<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixtureWorkFlow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $car = new \App\Model\Car(['name' => 'Машина']);
        $car->save();

        $user = new \App\Model\User(['name' => 'Имя', 'email' => 'email@email.ru', 'password' => '111']);
        $user->save();

        $userRent = new \App\Model\UserRent([
            'user_id' => $user->id,
            'car_id' => $car->id,
            'tariff_id' => 1,
            'rent_start' => '2018-07-01 06:30:00',
            'rent_finish' => '2018-07-02 02:45:00',
        ]);
        $userRent->save();

        $reservation1 = new \App\Model\UserRentSteps([
            'user_rent_id' => $userRent->id,
            'tariff_step_type_id' => 1,
            'step_start' => '2018-07-01 06:30:00',
            'step_finish' => '2018-07-01 07:00:00',
            'price' => 0,
            'amount' => 30,
            'cost' => 0,
        ]);
        $reservation1->save();

        $reservation2 = new \App\Model\UserRentSteps([
            'user_rent_id' => $userRent->id,
            'tariff_step_type_id' => 1,
            'step_start' => '2018-07-01 07:00:00',
            'step_finish' => '2018-07-01 07:30:00',
            'price' => 2,
            'amount' => 30,
            'cost' => 20,
        ]);
        $reservation2->save();

        $inspection = new \App\Model\UserRentSteps([
            'user_rent_id' => $userRent->id,
            'tariff_step_type_id' => 2,
            'step_start' => '2018-07-01 07:30:00',
            'step_finish' => '2018-07-01 07:35:00',
            'price' => 2,
            'amount' => 5,
            'cost' => 0,
        ]);
        $inspection->save();

        $driveTime1 = new \App\Model\UserRentSteps([
            'user_rent_id' => $userRent->id,
            'tariff_step_type_id' => 3,
            'step_start' => '2018-07-01 07:35:00',
            'step_finish' => '2018-07-01 08:40:00',
            'price' => 8,
            'amount' => 65,
            'cost' => 520,
        ]);
        $driveTime1->save();
        $driveLength1 = new \App\Model\UserRentSteps([
            'user_rent_id' => $userRent->id,
            'tariff_step_type_id' => 4,
            'step_start' => '2018-07-01 07:35:00',
            'step_finish' => '2018-07-01 08:40:00',
            'price' => 0,
            'amount' => 10,
            'cost' => 0,
        ]);
        $driveLength1->save();

        $driveParking1 = new \App\Model\UserRentSteps([
            'user_rent_id' => $userRent->id,
            'tariff_step_type_id' => 5,
            'step_start' => '2018-07-01 08:40:00',
            'step_finish' => '2018-07-01 08:50:00',
            'price' => 2,
            'amount' => 10,
            'cost' => 20,
        ]);
        $driveParking1->save();

        $driveTime2 = new \App\Model\UserRentSteps([
            'user_rent_id' => $userRent->id,
            'tariff_step_type_id' => 3,
            'step_start' => '2018-07-01 08:50:00',
            'step_finish' => '2018-07-01 09:30:00',
            'price' => 8,
            'amount' => 40,
            'cost' => 320,
        ]);
        $driveTime2->save();
        $driveLength2 = new \App\Model\UserRentSteps([
            'user_rent_id' => $userRent->id,
            'tariff_step_type_id' => 4,
            'step_start' => '2018-07-01 08:50:00',
            'step_finish' => '2018-07-01 09:30:00',
            'price' => 0,
            'amount' => 7,
            'cost' => 0,
        ]);
        $driveLength2->save();

        $driveParking2 = new \App\Model\UserRentSteps([
            'user_rent_id' => $userRent->id,
            'tariff_step_type_id' => 5,
            'step_start' => '2018-07-01 09:30:00',
            'step_finish' => '2018-07-01 18:00:00',
            'price' => 2,
            'amount' => 510,
            'cost' => 1020,
        ]);
        $driveParking2->save();

        $driveTime3 = new \App\Model\UserRentSteps([
            'user_rent_id' => $userRent->id,
            'tariff_step_type_id' => 3,
            'step_start' => '2018-07-01 18:00:00',
            'step_finish' => '2018-07-01 20:05:00',
            'price' => 8,
            'amount' => 125,
            'cost' => 1000,
        ]);
        $driveTime3->save();
        $driveLength3 = new \App\Model\UserRentSteps([
            'user_rent_id' => $userRent->id,
            'tariff_step_type_id' => 4,
            'step_start' => '2018-07-01 18:00:00',
            'step_finish' => '2018-07-01 20:05:00',
            'price' => 0,
            'amount' => 35,
            'cost' => 0,
        ]);
        $driveLength3->save();

        $driveParking3 = new \App\Model\UserRentSteps([
            'user_rent_id' => $userRent->id,
            'tariff_step_type_id' => 5,
            'step_start' => '2018-07-01 20:05:00',
            'step_finish' => '2018-07-01 23:00:00',
            'price' => 2,
            'amount' => 175,
            'cost' => 350,
        ]);
        $driveParking3->save();
        $driveParking4 = new \App\Model\UserRentSteps([
            'user_rent_id' => $userRent->id,
            'tariff_step_type_id' => 5,
            'step_start' => '2018-07-01 23:00:00',
            'step_finish' => '2018-07-01 24:00:00',
            'price' => 0,
            'amount' => 60,
            'cost' => 0,
        ]);
        $driveParking4->save();
        $driveParking5 = new \App\Model\UserRentSteps([
            'user_rent_id' => $userRent->id,
            'tariff_step_type_id' => 5,
            'step_start' => '2018-07-02 00:00:00',
            'step_finish' => '2018-07-02 01:10:00',
            'price' => 0,
            'amount' => 70,
            'cost' => 0,
        ]);
        $driveParking5->save();

        $driveTime4 = new \App\Model\UserRentSteps([
            'user_rent_id' => $userRent->id,
            'tariff_step_type_id' => 3,
            'step_start' => '2018-07-02 01:10:00',
            'step_finish' => '2018-07-02 02:45:00',
            'price' => 8,
            'amount' => 95,
            'cost' => 760,
        ]);
        $driveTime4->save();
        $driveLength4 = new \App\Model\UserRentSteps([
            'user_rent_id' => $userRent->id,
            'tariff_step_type_id' => 4,
            'step_start' => '2018-07-02 01:10:00',
            'step_finish' => '2018-07-02 02:45:00',
            'price' => 10,
            'amount' => 77,
            'cost' => 70,
        ]);
        $driveLength4->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
